<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://planviewer.nl
 * @since      1.0.0
 *
 * @package    Pvapidm
 * @subpackage Pvapidm/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Pvapidm
 * @subpackage Pvapidm/includes
 * @author     Planviewer <jp.gommers@planviewer.nl>
 */
class Pvapidm_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
