<?php

/**
 * Fired during plugin activation
 *
 * @link       http://planviewer.nl
 * @since      1.0.0
 *
 * @package    Pvapidm
 * @subpackage Pvapidm/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Pvapidm
 * @subpackage Pvapidm/includes
 * @author     Planviewer <jp.gommers@planviewer.nl>
 */
class Pvapidm_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
