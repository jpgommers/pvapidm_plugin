<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://planviewer.nl
 * @since      1.0.0
 *
 * @package    Pvapidm
 * @subpackage Pvapidm/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Pvapidm
 * @subpackage Pvapidm/public
 * @author     Planviewer <jp.gommers@planviewer.nl>
 */
class Pvapidm_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */


	public $responseObject;

	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Pvapidm_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Pvapidm_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/pvapidm-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Pvapidm_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Pvapidm_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/pvapidm-public.js', array( 'jquery' ), $this->version, false );

	}

    public function register_shorts()
    {
        // add_shortcode('gallery', 'gallery_func');
        add_shortcode('gallery', array($this,'gallery_func'));
    }

    public function gallery_func( $atts )
    {
        return "WAKAWAKA ". $atts['size'];
    }

    public function getAddress_func( $atts )
    {
        $url = "/product_api/v1/products/list";
        $post = true;
        $options = get_option($this->plugin_name);
        $call = new pvapidm_front();
        $response = $call->getProductOnAddress($options['productkey'], $options['productsecret'], $atts['hnr'], $atts['postcode'], $atts['toevoeging']);
        $response = $response['output'];
       // var_dump($response);
       //return json_decode($response['output']);
       // echo "<pre>".print_r($response[0]->intentions)."</pre>";
        $this->responseObject = $response;
       // return $this->responseObject;

    }

}
