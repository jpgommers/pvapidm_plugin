<?php
/**
 * Handles all API communication for the API's
 *
 *
 *
 * @package    Pvapidm
 * @subpackage Pvapidm/includes
 * @author     Planviewer <jp.gommers@planviewer.nl>
*/
class pvapidm_front
{

    public function getProductOnAddress($key = null, $secret = null, $hnr = null, $postcode = null, $toevoeging)
    {
       //lets build it stupid
        $query ="{
  \"entity\": \"adres\",
  \"query\": {
    \"bool\": {
      \"must\": [
        {
          \"field\": {
            \"fields\": [
              \"postcode\"
            ],
            \"query\": \"".str_replace(" ","",$postcode)."\"
          }
        },
        {
          \"field\": {
            \"method\": \"match\",
            \"fields\": [
              \"huisnummer\"
            ],
            \"query\": \"".$hnr."\"
          }
        }
      ]
    }
  }
}";


        $url = "https://www.planviewer.nl/product_api/v1/search/structured";
        $jsonQuery = $this->connect(true, $query, $key, $secret, $url);
        $array = json_decode($jsonQuery['output']);
        if(sizeof($array) == 1)
        {
            return $this->getProducts($array[0]->uuid, $key, $secret);
        }

        for($i = 0; $i< sizeof($array); $i++)
        {
            if($array[$i]->properties->huisnummer == $hnr && $array[$i]->properties->huisletter == strtoupper($toevoeging))
            {
                return $this->getProducts($array[$i]->uuid,$key,$secret);
            }
        }
    }


    private function connect( $post = false, $string='', $key, $secret, $url)
    {
        $headers = array(
            'Content-Type:application/json',
            'Authorization: Basic '. base64_encode("$key:$secret")
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if($post) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $string);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Host: planviewer.nl'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $output = curl_exec($ch);
        $info = curl_getinfo($ch);
        if (curl_errno($ch)) {
            // this would be your first hint that something went wrong
            die('Couldn\'t send request: ' . curl_error($ch) .' Key: '.$key." Secret: ".$secret);
        }
        curl_close($ch);

        return array('output' => $output, 'info' => $info);
    }

    private function getProducts($uuid, $key, $secret)
    {
        //get a list of available producttypes
        $url = "https://www.planviewer.nl/product_api/v1/products/list";
        $products = $this->connect(false,"",$key,$secret,$url);
        $productList = json_decode($products['output']);
        //build product array for products for this location
        $productArray = array();
        for($i =0; $i < sizeof($productList); $i++)
        {
            $productArray[] = $productList[$i]->slug;
        }
        //do actual product intentions
        $url = "https://www.planviewer.nl/product_api/v1/intentions/list";
        $string = json_encode(array("uuids" => array($uuid), "products" => $productArray));
        return $this->connect(true,$string,$key, $secret, $url);

    }
}

