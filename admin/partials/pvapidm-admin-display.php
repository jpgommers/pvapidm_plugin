<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://planviewer.nl
 * @since      1.0.0
 *
 * @package    Pvapidm
 * @subpackage Pvapidm/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="wrap">

    <h2><?php echo esc_html(get_admin_page_title()); ?></h2>

    <form method="post" name="cleanup_options" action="options.php">
	    <?php
	    //Grab all options
	    $options = get_option($this->plugin_name);


	    print_r($options);

	    //get api info
        $_productkey = $options['productkey'];
        $productsecret = $options['productsecret'];

        $mapkey = $options['mapkey'];
        $mapsecret = $options['mapsecret'];

	    // Cleanup
	    $cleanup = $options['cleanup'];
	    $comments_css_cleanup = $options['comments_css_cleanup'];
	    $gallery_css_cleanup = $options['gallery_css_cleanup'];
	    $body_class_slug = $options['body_class_slug'];
	    $jquery_cdn = $options['jquery_cdn'];




	    ?>

	    <?php
            settings_fields($this->plugin_name);
	        do_settings_sections($this->plugin_name);
	    ?>

        <!-- remove some meta and generators from the <head> -->
        <table class="form-table">
            <tbody>
            <!--product api basic http settings-->
            <tr>
                <td colspan="2" class="formheader"><h3>Product API Settings</h3></td>
            </tr>
            <tr>
                <td><label for="<?php echo $this->plugin_name; ?>-productkey"><?php _e("Access key", $this->plugin_name);?>
                    </label> </td>
                <td><input type="text" placeholder="access key" id="<?php echo $this->plugin_name; ?>-productkey"
                           name="<?php echo $this->plugin_name; ?>[productkey]" class="regular-text"
                           value="<?php if(!empty($access_productkey)){echo $access_productkey;} ?>" ></td>
            </tr>

            <tr>
                <td><label for="<?php echo $this->plugin_name; ?>-productsecret"><?php _e("Secret", $this->plugin_name);?>
                    </label> </td>
                <td><input type="password" placeholder="secret" id="<?php echo $this->plugin_name; ?>-productsecret"
                           name="<?php echo $this->plugin_name; ?>[productsecret]" class="regular-text"
                           value="<?php if(!empty($productsecret)){echo $productsecret;} ?>"></td>
            </tr>
            <tr><td colspan="2" ><label><input type="checkbox" onclick="showpass('<?php echo $this->plugin_name; ?>-productsecret')">
                        Show Password </label></td></tr>
            <!--Map api basic http settings-->

            <tr>
                <td colspan="2" class="formheader"><h3>Map API Settings</h3></td>
            </tr>
            <tr>
                <td><label for="<?php echo $this->plugin_name; ?>-mapkey"><?php _e("Access key", $this->plugin_name);?>
                    </label> </td>
                <td><input type="text" placeholder="access key" id="<?php echo $this->plugin_name; ?>-mapkey"
                           name="<?php echo $this->plugin_name; ?>[mapkey]" class="regular-text"
                           value="<?php if(!empty($access_mapkey)){echo $access_mapkey;} ?>" ></td>
            </tr>
            <tr>
                <td><label for="<?php echo $this->plugin_name; ?>-mapsecret"><?php _e("secret", $this->plugin_name);?>
                    </label> </td>
                <td><input type="password" placeholder="secret" id="<?php echo $this->plugin_name; ?>-mapsecret"
                           name="<?php echo $this->plugin_name; ?>[mapsecret]" class="regular-text"
                           value="<?php if(!empty($mapsecret)){echo $maposecret;} ?>"></td>

            </tr>
            <tr><td colspan="2" ><label><input type="checkbox" onclick="showpass('<?php echo $this->plugin_name; ?>-mapsecret')">
                    Show Password </label></td></tr>

            </tbody>
        </table>



	    <?php submit_button(__('Save all changes', $this->plugin_name), 'primary','submit', TRUE); ?>

    </form>
<div class="viewer">
    <iframe src="https://www.planviewer.nl/maps_api/v1/embed/editor/471d4cad1cd3e82e9b7618aaf98ee73761d7c7045e32d52cf462e81996de61ce">
    </iframe>
</div>

</div>

<script type="text/javascript">

    function showpass(id) {
        var x = document.getElementById(id);
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }

    }

</script>
