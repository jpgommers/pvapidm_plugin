<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://planviewer.nl
 * @since             1.0.0
 * @package           Pvapidm
 *
 * @wordpress-plugin
 * Plugin Name:       Planviewer API demo
 * Plugin URI:        http://planviewer.nl
 * Description:       Demo voor het werken met Planviewer's Product- & Maps-API
 * Version:           1.0.0
 * Author:            Planviewer
 * Author URI:        http://planviewer.nl
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       pvapidm
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-pvapidm-activator.php
 */
function activate_pvapidm() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-pvapidm-activator.php';
	Pvapidm_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-pvapidm-deactivator.php
 */
function deactivate_pvapidm() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-pvapidm-deactivator.php';
	Pvapidm_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_pvapidm' );
register_deactivation_hook( __FILE__, 'deactivate_pvapidm' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-pvapidm.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_pvapidm() {

	$plugin = new Pvapidm();
	$plugin->run();

}
run_pvapidm();
