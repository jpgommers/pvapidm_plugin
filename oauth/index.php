<?php

  // Require composer autoloader
  require __DIR__ . '/vendor/autoload.php';

  require __DIR__ . '/dotenv-loader.php';

  use Auth0\SDK\Auth0;

putenv("AUTH0_CALLBACK_URL=http://demosite.test/app/plugins/pvapidm/oauth/");

$domain        = getenv('AUTH0_DOMAIN');
$client_id     = getenv('AUTH0_CLIENT_ID');
$client_secret = getenv('AUTH0_CLIENT_SECRET');
$redirect_uri  = getenv('AUTH0_CALLBACK_URL');
$audience      = getenv('AUTH0_AUDIENCE');


if($audience == ''){
    $audience = 'https://' . $domain . '/userinfo';
  }

$auth0 = new Auth0([
	'domain' => 'planviewer.eu.auth0.com',
	'client_id' => 'ozlyAUQfUdQyOkZqTR0DS89lIBdDpU6t',
	'client_secret' => 'pQT4n339Kx8OrznyejwTO3qVqHsBV_a8MlW_9_0Nid-Ji7W2Al6FWSXNxF0XNTzq',
	'redirect_uri' => 'http://demosite.test/app/plugins/pvapidm/oauth/',
	'audience' => 'https://planviewer.eu.auth0.com/userinfo',
	'scope' => 'openid profile',
	'persist_id_token' => true,
	'persist_access_token' => true,
	'persist_refresh_token' => true,
]);

  // index.php


$userInfo = $auth0->getUser();

$debug = array();

if (!$userInfo) {
    // We have no user info
    // redirect to Login
    $debug['msg'] =  'Error: No user found';

} else {
    // User is authenticated
    // Say hello to $userInfo['name']
    // print logout button
    $debug['msg'] = 'Succes: We\'ve got him!';
    $debug['data'] = $userInfo;
}


?>
<html>
    <head>
        <script src="http://code.jquery.com/jquery-3.1.0.min.js" type="text/javascript"></script>

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- font awesome from BootstrapCDN -->
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">

        <link href="public/app.css" rel="stylesheet">



    </head>
    <body class="home">
    <div style="background-color: black; color: yellow; width: auto;"><pre><?php print_r($debug); ?></pre></div>
        <div class="container">
            <div class="login-page clearfix">
              <?php if(!$userInfo): ?>
              <div class="login-box auth0-box before">
                <img src="https://i.cloudup.com/StzWWrY34s.png" />
                <h3>Auth0 Example</h3>
                <p>Zero friction identity infrastructure, built for developers</p>
                <a class="btn btn-primary btn-lg btn-login btn-block" href="login.php">Sign In</a>
              </div>
              <?php else: ?>
              <div class="logged-in-box auth0-box logged-in">
                <h1 id="logo"><img src="//cdn.auth0.com/samples/auth0_logo_final_blue_RGB.png" /></h1>
                <img class="avatar" src="<?php echo $userInfo['picture'] ?>"/>
                <h2>Welcome <span class="nickname"><?php echo $userInfo['nickname'] ?></span></h2>
                <a class="btn btn-warning btn-logout" href="logout.php">Logout</a>
              </div>
              <?php endif ?>
            </div>
        </div>
    </body>
</html>


